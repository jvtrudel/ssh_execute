# SSH execute

Helper scripts to execute commands on a remote througth ssh


# Usage

## Functions to inspect and manage local ssh environment and objects

    local_ssh _host

    local_ssh has_host

## Functions to inspect and manage local ssh environment and objects


    remote_ssh check_connection -H my_vps -p passwordfile

  
## Execution on remote 

    cat <<-EOM | ssh_execute -H my_vps -p passwordfile
      lsb_release
      whoami
      pwd
    EOM

## Installation

Unless you provide 'fatal', 'error' and ... functions, you need `gitlab.com:jvtrudel/shelp`.

   git clone git@gitlab.com:jvtrudel/ssh_execute ~/.shelp/repo/ssh_execute


Then, you can source it

    . <(shelp -s gen core) <(shelp -u gen ssh_execute)

 

